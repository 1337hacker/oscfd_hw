import os, shutil, random, stat

numberOfSimulations = 3

for j in range(numberOfSimulations):
	#Name of new folder
	Name = 'cavity'+str(j)
	#Copy the case template
	if os.path.exists('./'+Name):
		shutil.rmtree('./'+Name)
	shutil.copytree('cavity',Name)

	#Number of boxes and refinement regions
	numberOfRefinements = random.randint(2,10)

	#---------------------------------------------------------------------
	#Create the topoSetDicts for the boxes
	
	#Create the topoSetDicts for the refinementRegions
	for i in range(numberOfRefinements):

		s='''/*--------------------------------*- C++ -*----------------------------------*\
	  =========                 |
	  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
	   \\    /   O peration     | Website:  https://openfoam.org
	    \\  /    A nd           | Version:  7
	     \\/     M anipulation  |
	\*---------------------------------------------------------------------------*/
	FoamFile
	{
	    version     2.0;
	    format      ascii;
	    class       dictionary;
	    object      topoSetDict;
	}

	actions
	(
	    {
		name    c0;
		type    cellSet;
		action  new;
		source  boxToCell;
		sourceInfo
		{
		    box (x1 y1 0) (x2 y2 0.1);
		}
	    }


	);'''
		x1=random.randint(0,20)*float(0.05)
		x2=x1+random.randint(-10,10)*float(0.05)	
		y1=random.randint(0,20)*float(0.05)
		y2=y1+random.randint(-10,10)*float(0.05)
		s = s.replace('x1',str(min(x1,x2)))
		s = s.replace('x2',str(max(x1,x2)))
		s = s.replace('y1',str(min(y1,y2)))
		s = s.replace('y2',str(max(y1,y2)))
		f = open('./'+Name+'/system/Refinements.'+str(i+1),'w')
		f.write(s)
		f.close()



		



	#--------------------------------------------------------------------------------------------------
	#Create the Allrun script
	#-----------------------

	s ='''#!/bin/sh
	# Source tutorial run functions
	. $WM_PROJECT_DIR/bin/tools/RunFunctions

	runApplication blockMesh

	# Create the refinements
	i=1
	while [ "$i" -lt numberOfRefinements ] ; do # -lt is less then (strictly)



	    
	    cp system/Refinements.${i} system/topoSetDict
	    runApplication topoSet
	    runApplication refineMesh -overwrite
	    mv log.topoSet log.Refinements.${i}
	    mv log.refineMesh log.refineMesh.${i}
	    i=`expr $i + 1`
	done

	runApplication icoFoam
	'''      
	s = s.replace('numberOfRefinements',str(numberOfRefinements+1))
	f = open('./'+Name+'/Allrun','w')
	f.write(s)
	f.close()
	os.chmod('./'+Name+'/Allrun',stat.S_IRWXU)

	#Run the simulation
	os.chdir(Name)
	os.system('./Allrun')
	os.system('foamToVTK')
	os.system('sed -i "s/cavity0/'+Name+'/g" savetocsv.py')
	os.system('pvpython savetocsv.py')
	os.chdir('..')

