# trace generated using paraview version 5.6.3
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

ResetSession()

# create a new 'XML MultiBlock Data Reader'
cavity0_100vtm = XMLMultiBlockDataReader(FileName=['/home/e/OpenFOAM/e-v1912/run/cavity0/VTK/cavity0_100.vtm'])
cavity0_100vtm.CellArrayStatus = ['initialResidual:p', 'p', 'U']
cavity0_100vtm.PointArrayStatus = ['initialResidual:p', 'p', 'U']

# Properties modified on cavity0_100vtm
cavity0_100vtm.CellArrayStatus = []
cavity0_100vtm.PointArrayStatus = ['initialResidual:p']

# save data
SaveData('/home/e/OpenFOAM/e-v1912/run/cavity0/cavity0.csv', proxy=cavity0_100vtm)

#### uncomment the following to render all views
# RenderAllViews()
# alternatively, if you want to write images, you can use SaveScreenshot(...).
